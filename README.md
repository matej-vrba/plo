Plo is a a small utility for testing packet drop.
Launching without arguments launches a server, using `-s` argument allows specifying server address and launches in client mode.

It has more options documented in help text (`-h`).

# Building

Plo is written in rust, so you need rust toolchain to build it, I've tested it on x86_64 and arm64 on linux, don't know how well it works on other platforms.

# Output

Client prints statistics in this format:

```
PS:      345 BS:       252899 PR:      344 BR:       252780 loss: 0.000%
```

- `PS` - Packets sent
- `BS` - Bytes sent (only data packets, doesn't include L1-L3 headers)
- `PR` - Packets received
- `BR` - Bytes received (also doesn't include headers)
- `loss` - packet loss in percents
