use clap::Parser;
use rand::Rng;
use std::net::UdpSocket;
use std::time::Duration;
use std::{
    io::{self, Write},
    thread,
};

// NOTE: server has hardcoded 1500 byte maximum, maybe I'll fix this latex when I need to use it on non-ethernet  (or ethernet with custom mtu)

#[derive(Parser, Debug)]
#[command(version)]
struct Args {
    #[arg(
        short,
        long,
        help = "Server to connect to, if not specified operates as server"
    )]
    server_address: Option<String>,
    #[arg(
        short,
        long,
        default_value_t = 1122,
        help = "Port on which to connect/listen"
    )]
    port: u16,
    #[arg(
        short,
        long,
        default_value_t = 1.0,
        help = "Seconds between sending each packet"
    )]
    interval: f32,
    #[arg(
        short = 'm',
        long,
        default_value_t = 100,
        help = "Minimum data size of each packet"
    )]
    min_size: i16,
    #[arg(
        short = 'M',
        long,
        default_value_t = 1400,
        help = "Maximum data size of each packet"
    )]
    max_size: i16,
}

fn main() {
    let args = Args::parse();

    if args.min_size > args.max_size {
        eprintln!("Error: minimum specified size is larger than maximum size");
        return;
    }

    let sock = open_socket(&args).unwrap();

    if args.server_address.is_some() {
        do_client_stuff(sock, args).unwrap();
    } else {
        do_server_stuff(sock);
    };
}

fn open_socket(args: &Args) -> Result<UdpSocket, String> {
    let sock = if args.server_address.is_some() {
        //client
        UdpSocket::bind("0.0.0.0:0")
    } else {
        println!("Starting serveer on 0.0.0.0:{}", args.port);
        //server
        UdpSocket::bind(format!("0.0.0.0:{}", args.port))
    };
    match sock {
        Ok(sock) => return Ok(sock),
        Err(e) => {
            return Err(String::from(format!(
                "Failed to open udp socket: {}",
                e.to_string()
            )))
        }
    }
}

fn do_client_stuff(sock: UdpSocket, args: Args) -> Result<(), String> {
    let addr = if let Some(addr) = args.server_address {
        addr
    } else {
        return Err(String::from("Error: missing server address"));
    };
    if let Err(e) = sock.connect(format!("{}:{}", addr.clone(), args.port)) {
        return Err(format!("Failed to connect to {}: {}", addr, e.to_string()));
    }
    let vals: Vec<u8> = (0..1500).map(|v| ((v + 1000) % 256) as u8).collect();
    let mut rng = rand::thread_rng();
    sock.set_nonblocking(true).unwrap();
    let mut recv_buf = [0; 1500];

    let mut packets_sent: u64 = 0;
    let mut packets_recvd: u64 = 0;
    let mut bytes_sent: u64 = 0;
    let mut bytes_recvd: u64 = 0;

    loop {
        let to_send_size = rng.gen_range(args.min_size..=args.max_size) as usize;
        match sock.send(&vals[0..to_send_size]) {
            Ok(_) => {}
            Err(e) => {
                if e.kind() != io::ErrorKind::WouldBlock {
                    panic!("send error: {}", e.to_string());
                }
            }
        }
        bytes_sent += to_send_size as u64;
        packets_sent += 1;

        loop {
            match sock.recv(&mut recv_buf) {
                Ok(bytes_just_received) => {
                    bytes_recvd += bytes_just_received as u64;
                    packets_recvd += 1;
                }
                Err(e) => {
                    if e.kind() != io::ErrorKind::WouldBlock {
                        panic!("Recv error: {}", e.to_string());
                    }
                    break;
                }
            }
        }

        print!("\x08 \x08\x08 \x08\x08 \x08\x08 \x08\x08 \r");

        print!(
            "PS: {: >8} BS: {: >12} PR: {: >8} BR: {: >12} loss: {:.3}%",
            packets_sent,
            bytes_sent,
            packets_recvd,
            bytes_recvd,
            100. * (1. - (packets_recvd + 1) as f64 / packets_sent as f64),
        );
        io::stdout().flush().unwrap();
        thread::sleep(Duration::from_millis((args.interval * 1000.) as u64));
    }
}
fn do_server_stuff(sock: UdpSocket) {
    let mut buf = [0; 1500];
    loop {
        let (number_of_bytes, src_addr) = sock.recv_from(&mut buf).unwrap();
        sock.send_to(&buf[0..number_of_bytes], src_addr).unwrap();
    }
}
